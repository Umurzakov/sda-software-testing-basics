package ee.sda.testingbasics;

import java.util.Arrays;

public class RemoveDuplicateStrings {

    public String[] removeDuplications(String[] fruits){

        if(fruits == null || fruits.length == 0){
            return new String[]{};
        }

        String[] tempArray = new String[fruits.length];

        int counter = 0;

        // apple, apple, ...
        for (String fruit:fruits){
            boolean found = false;
            for (int i = 0; i < tempArray.length; i++) {
                if(fruit.equals(tempArray[i])){
                    found = true;
                    break;
                }
            }

            if(!found) {
                tempArray[counter] = fruit;
                counter++;
            }
        }

        String[] out = new String[counter];

        for (int i = 0; i < tempArray.length; i++) {
            if(tempArray[i] != null) {
                out[i] = tempArray[i];
            }
        }

        return out;
    }
}
