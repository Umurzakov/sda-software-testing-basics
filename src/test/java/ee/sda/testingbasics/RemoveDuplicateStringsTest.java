package ee.sda.testingbasics;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


// Junit 5 - jupiter
public class RemoveDuplicateStringsTest {

    @Test
    public void removeDuplicatesSuccessTest(){

        RemoveDuplicateStrings removeDuplicateStrings = new RemoveDuplicateStrings();

        String[] fruitInput = {"apple", "apple", "strawberry", "Watermelon"};
        String[] expectedFruitOutput = {"apple", "strawberry", "Watermelon"};

        String[] actualFruitOutput =  removeDuplicateStrings.removeDuplications(fruitInput);

        Assertions.assertArrayEquals(expectedFruitOutput, actualFruitOutput);
    }

    @Test
    public void removeDuplicatesFailureEmptyArrayTest(){

        RemoveDuplicateStrings removeDuplicateStrings = new RemoveDuplicateStrings();

        String[] fruitInput = {};
        String[] expectedFruitOutput = {};

        String[] actualFruitOutput =  removeDuplicateStrings.removeDuplications(fruitInput);

        Assertions.assertArrayEquals(expectedFruitOutput, actualFruitOutput);
    }

    @Test
    public void removeDuplicatesFailureArrayNullTest(){

        RemoveDuplicateStrings removeDuplicateStrings = new RemoveDuplicateStrings();

        String[] fruitInput = null;
        String[] expectedFruitOutput = {};

        String[] actualFruitOutput =  removeDuplicateStrings.removeDuplications(fruitInput);

        Assertions.assertArrayEquals(expectedFruitOutput, actualFruitOutput);
    }
}
