package ee.sda.testingbasics;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class CalculatorTest {

    // Annotations
    // Test method
    @Test
    @DisplayName("Add and multiply test example")
    public void addAndMultiplyByThreeTest(){

        Calculator calculator = new Calculator();

        //I changed something

        int actualResult = calculator.AddAndMultiplyByThree(2,2);

        // Positive test example
        Assertions.assertEquals(12, actualResult, "Actual result should be equal to 12");
        // Negative test example 1:
        Assertions.assertNotEquals(10, actualResult, "");
        // Negative test example 2:
        // Result should not be negative value
        Assertions.assertNotEquals(-10, actualResult, "");

        // BDD vs TDD
        // Write logic and test means -> BDD
        // We first write a test only then we start to implement logic -> TDD

        // TDD cycle:
        // The first test should always fail(After you wrote test)
        // Once it failed, you will implement or improve your logic
        // Run it again

        // Unit test

        // Single responsibility:
        // Every single test should test only one functionality
    }

    // Removing Duplicates
    // {"apple", "apple", "strawberry", "Watermelon"}
    // {"apple", "strawberry", "Watermelon"}
}
